<?php
header("Access-Control-Allow-Origin: *");
include('includes/header.inc.php');
include('includes/texts/'.$_POST['lang_param'].'.php');
session_start();
$aSuccess = false;
$aResult = "";
$aMsg = '';

switch ($_POST["request"])
{
    case 'ajaxsendemailprocess':
		
		$str_url_confirmation = '';
		
		if(!validate_email_address($aConfig['main']['id'],$GLOBALS["db"]->escapeString($_POST['var_email']),$GLOBALS["db"]->escapeString($_POST['var_register']))){
		
			$str_code = '';
			$str_code = generate_validation_code($aConfig['main']['id']);
			
			$aArray = array (
				'name' => $GLOBALS["db"]->escapeString($_POST['var_name']),
	            'email' => $GLOBALS["db"]->escapeString($_POST['var_email']),
	            'points' => $aPrizePoints[$_POST['var_register']],
	            'code' => $str_code,
	            'reg_type' => $GLOBALS["db"]->escapeString($_POST['var_register']),
	            'reg_confirmed' => 1,
	            'language' => $_POST['lang_param'],
			);
			
			$retValue = insert_email_record($aConfig['main']['id'],json_encode($aArray));
			
			$str_url_confirmation = $_POST['var_base_url'].'index.php?conf='.$str_code;
			sendemailprocess($GLOBALS["db"]->escapeString($_POST['var_email']),$GLOBALS["db"]->escapeString($_POST['var_register']),$str_url_confirmation, $_POST['lang_param']);
			
			$aSuccess = true;
			$aMsg = trim($aTexts['err']['email_send']);
			
		} 
		else if( check_unsubscribed_user($aConfig['main']['id'],$GLOBALS["db"]->escapeString($_POST['var_email'])) ){		
			$str_code = '';
			$str_code = generate_validation_code($aConfig['main']['id']);
			
			$arr_code_results = searchEmailToUnsubscribe($aConfig['main']['id'],trim($GLOBALS["db"]->escapeString($_POST['var_email'])));
			$arr_existing_dets = json_decode($arr_code_results[0]['data'],true);
			
			$aArray = array (
						'email' => $arr_existing_dets['email'],
						'points' => $aPrizePoints[$arr_existing_dets['reg_type']],
						'code' => $arr_existing_dets['code'],
						'reg_type' => $arr_existing_dets['reg_type'],
						'reg_confirmed' => 1,
						'language' => $_POST['lang_param'],
					   );
			
			update_unsubscribe_email(json_encode($aArray),$arr_code_results[0]['id']);
			
			$str_url_confirmation = $_POST['var_base_url'].'index.php?conf='.$str_code;
			sendemailprocess($GLOBALS["db"]->escapeString($_POST['var_email']),$GLOBALS["db"]->escapeString($_POST['var_register']),$str_url_confirmation,$_POST['lang_param']);
			
			$aSuccess = true;
			$aMsg = trim($aTexts['err']['email_send']);
		}
		else {
			$aSuccess = false;
			$aMsg = trim($aTexts['err']['email_fail']);
		}
				
		break;
		
	case 'ajaxprocessvalidationcode':
		
		$arr_code_results = validate_confimation_code($aConfig['main']['id'],trim($GLOBALS["db"]->escapeString($_POST['var_code'])));
		$arr_existing_dets = json_decode($arr_code_results['mc_value'],true);
		
		if($arr_existing_dets['reg_confirmed'] == 0){
		
			$aArray = array (
			            'email' => $arr_existing_dets['email'],
			            'points' => $aPrizePoints[$arr_existing_dets['reg_type']],
			            'code' => $arr_existing_dets['code'],
			            'reg_type' => $arr_existing_dets['reg_type'],
			            'reg_confirmed' => 1,
						'language' => $_POST['lang_param'],
					   );
			
			update_confirm_registration(json_encode($aArray),$arr_code_results['mc_id']);
			
			$aSuccess = true;
			$aMsg = trim($aTexts['err']['email_conf']);
		
		} else {
			$aSuccess = true;
			$aMsg = trim($aTexts['err']['email_conf_2']);
			 
		}
		
		break;

	
	case 'ajaxprocessunsubscribeemail':
		
		$arr_code_results = searchEmailToUnsubscribe($aConfig['main']['id'],trim($GLOBALS["db"]->escapeString($_POST['var_code'])));
		$arr_existing_dets = json_decode($arr_code_results[0]['data'],true);
		$aSuccess = false;
		if(empty($arr_existing_dets['deleted'])){		
			$aArray = array (
						'email' => $arr_existing_dets['email'],
						'points' => $aPrizePoints[$arr_existing_dets['reg_type']],
						'code' => $arr_existing_dets['code'],
						'reg_type' => $arr_existing_dets['reg_type'],
						'reg_confirmed' => 0,
						'deleted' => 1,
						'language' => $arr_existing_dets['language'],
					   );
			
			update_unsubscribe_email(json_encode($aArray),$arr_code_results[0]['id']);
			$aMsg = trim($aTexts['err']['email_unsubscribe']);
			$aSuccess = true;
		}
			
		
		break;

	case 'fbInsertRecord':
		
		$aArray = array (
				'email' => '',
				'points' => $aPrizePoints[$_POST['var_register']],
				'code' => 0,
				'reg_type' => $GLOBALS["db"]->escapeString($_POST['var_register']),
				'reg_confirmed' => 1,
				'language' => $_POST['lang_param'],
			);

		$retValue = insert_email_record($aConfig['main']['id'],json_encode($aArray));
		$aSuccess = true;
		$aMsg = $aTexts['err']['facebook_share'];

		break;

	case 'twtInsertRecord':
			
		$aArray = array (
				'email' => '',
				'points' => $aPrizePoints[$_POST['var_register']],
				'code' => 0,
				'reg_type' => $GLOBALS["db"]->escapeString($_POST['var_register']),
				'reg_confirmed' => 1,
				'language' => $_POST['lang_param'],
			);

		$retValue = insert_email_record($aConfig['main']['id'],json_encode($aArray));
		$aSuccess = true;
		$aMsg = $aTexts['err']['twitter_share'];

		break;
		
	case 'insertKoreaRecord':
		if(!validate_email_address($aConfig['main']['id'],$GLOBALS["db"]->escapeString($_POST['var_mobilenumber']),$GLOBALS["db"]->escapeString($_POST['var_register']))){
			$aArray = array (
					'mobile' => $_POST['var_mobilenumber'],
					'platform' => $_POST['var_platform'],
					'points' => $aPrizePoints[$_POST['var_register']],
					'code' => 0,
					'reg_type' => $GLOBALS["db"]->escapeString($_POST['var_register']),
					'reg_confirmed' => 1,
					'language' => $_POST['lang_param'],
				);

			$retValue = insert_email_record($aConfig['main']['id'],json_encode($aArray));
			$aSuccess = true;
			$aMsg = $aTexts['err']['email_send'];
		}else{
			$aSuccess = false;
			$aMsg = trim($aTexts['err']['email_fail']);
		}
		
		break;		
	
	case 'get_total_points':
		
		$total_count = get_total_points($aConfig['main']['id']);
		$aResult = $total_count;
		
		break;
		
	case 'ajaxprocessaddpoints':
		
			$arr_results = validate_admin_points($aConfig['main']['id']);
			
			if($arr_results > 0){
				
				$arr_manual_points = get_admin_points_arr($aConfig['main']['id']);
				
				$aArray2 = array (
		            'email' => '',
		            'points' => ($arr_manual_points['mpoints'] + $_POST['var_points']),
		            'code' => '',
		            'reg_type' => $GLOBALS["db"]->escapeString('admin'),
		            'reg_confirmed' => 1,
				);
				
				update_manual_points(json_encode($aArray2),$arr_manual_points['mid']);
				
			} else {
				
				$aArray = array (
		            'email' => '',
		            'points' => $_POST['var_points'],
		            'code' => '',
		            'reg_type' => $GLOBALS["db"]->escapeString('admin'),
		            'reg_confirmed' => 1,
				);
				
				insert_email_record($aConfig['main']['id'],json_encode($aArray));
			}
			
		break;
		
	case 'ajaxprocesslogout':
			$_SESSION["logged"] = false;
		break;
		
	
}

echo json_encode(array('success' => $aSuccess, 'data' => $aResult,'msg'=> $aMsg));

?>