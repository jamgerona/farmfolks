<?php

include ('includes/header.inc.php');

ini_set('memory_limit', '220M');

// Include PEAR::Spreadsheet_Excel_Writer

//set_include_path(implode(PATH_SEPARATOR, array('.', '../minisites/despicable-me-2/PEAR', get_include_path())));
//set_include_path(":/var/www/new-staging.gameloft.org/php-includes:/usr/share/php:/var/www/php_common/inter_web_iphone:/var/www/php_common/:/var/www/new-staging.gameloft.org/documents/minisites/despicable-me-2/PEAR2");
//set_include_path("PEAR");
//require_once "Spreadsheet/Excel/Writer.php";

/** Error reporting */

error_reporting(E_ALL);

/////////////////////////////////////////////////////////////

// Create an instance
//$xls =& new Spreadsheet_Excel_Writer();

// Send HTTP headers to tell the browser what's coming
//$xls->send('list Despicable Me 2 - '.date('d-M-Y G_i_s').'.xls');

//$sheet =& $xls->addWorksheet('Users ');

	$sSql = 'SELECT *
			 FROM `minisite_contest`
			 WHERE `mc_minisite` = "74"
			 AND  `mc_value`  LIKE \'%"reg_confirmed":1%\'
			 ORDER BY `mc_timestamp` ASC';
	
	$r = $GLOBALS["db"]->query($sSql); 
	
	$aRow = $GLOBALS["db"]->fetchAssoc($r);
	
	$aUsers = array ();
	while ( $aRow = $GLOBALS["db"]->fetchAssoc($r) )
	{
		$aUsers[] = $aRow;
	}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Users</title>
	<script type="text/javascript" src="https://media01.gameloft.com/common_scripts/jquery/1.4.2.min/jquery.js"></script>
	<script language="javascript">
	$(document).ready(function() 
	{
	     $(".botonExcel").click(function(event) 
	     {
	     	 event.preventDefault();
		     $("#datasend").val( $("<div>").append( $("#excel").eq(0).clone()).html());
		     $("#formexport").submit();
		 });
	});
	</script>

</head>
<body>
	<center>
	<form action="Excel.php" method="post" target="_blank" id="formexport">
	<p class="botonExcel" style="cursor: hand;"><u>Click Here to Export</u></p>
	<input type="hidden" id="datasend" name="datasend" />
	</form>
	
	<div>
	<table border="1" bordercolor="black" id="excel" width="700" cellpadding="2" cellspacing="2">
		<th bgcolor="#A9A9F5">Email</th>
		<th bgcolor="#A9A9F5">Date</th>
		<?php
		foreach( $aUsers AS $aData ){
			$aTextUser = json_decode($aData['mc_value'],true);
			if(!empty($aTextUser['email'])){
		?>
		<tr border="1" bordercolor="black">
			<td><?php echo $aTextUser['email']; ?></td>
			<td><?php echo $aData['mc_timestamp']; ?></td>
		</tr>		
		<?php
			}
		}?>
	</table>
	</div>
	</center>
	
</body>

