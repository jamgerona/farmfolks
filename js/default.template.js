$(document).ready(function(){
	init();
})
function init() {
	// FB SHARE
	startFB();
	fbclick();
	// TW Tweet
	startTW();
	// twclick();
	// ENLIST NOW
	enlistnow();
}
function startFB(){
	FB.init({
		appId: FB_APP_ID, 
		status: true,
    	cookie: true, 
    	xfbml: true
    });
}
function fbclick() {
	$("#fbshare").click(function(e){
		e.preventDefault();
		fbInstallApp(language_decode);
	})
}
function fbInstallApp(language){
	FB.getLoginStatus(function(response) {
	
		if(response.status != 'connected'){
			FB.login(function(response) {
				
				if (response.authResponse != null) {
					FB.api('/me', function(response) {
						fbShareDialog(language);
					});
				}
			}, {scope: 'email,user_likes'});
		}else {
			fbShareDialog(language);
		}
	});
}

function fbShareDialog(language){	
	FB.getLoginStatus(function(response) {
			
		FB.ui({
			method: 'feed',
			picture: URL_DINAMIC_CONTENT+'images/page_template/fbimg.png',
			name: FB_TITLE_NAME, 
			link:  URL_DINAMIC_CONTENT,
			description: FB_DESC+ ' ' + xShort_url
			// caption: FB_MSG,
		},
		function(response){
			if(response != null){
				FB.api('/me', function(response) {
					fbInsertRecord(response.email,language);
				});	
			}
		});
	});
}

function fbInsertRecord(facebookEmailAdd,language){
	$.ajax({
		url : base_url+'ajax.php',
		type : 'POST',
		data : 'request=fbInsertRecord&var_email='+facebookEmailAdd+'&var_register=facebook&lang_param='+language,
		dataType : 'json',
		success : function(data) {
			showdialog(data.msg);
		}
	});	
}

function startTW() {
	twttr.events.bind('tweet', function(event){
		twtInsertRecord(language_decode);
	});	
}
function twtInsertRecord(language){
	$.ajax({
		url : base_url+'ajax.php',
		type : 'POST',
		data : 'request=twtInsertRecord&var_email=none&var_register=twitter&lang_param='+language,
		dataType : 'json',
		success : function(data) {
			showdialog(data.msg);	
		}
	});	
}
function enlistnow() {
	$("#enlist-form").submit(function(e){
		e.preventDefault();
		var field_name = $(this).find("#name").val();
		var field_email = $(this).find("#email").val();
		var hasclass = $(this).find(".f-s-tickbox").hasClass("tick-active");
		if(empty($.trim(field_email))) {
			showdialog(xEmailMsgEmpty);
		} 
		else if (empty($.trim(field_name))) {
			showdialog(xNameMsgEmpty);
		}
		else {
			if (!hasclass) {
				showdialog(xPrivacy_Policy);
			} else {
				sendemail(field_name,field_email,'website',language_decode);
			} 	
		}
	})
}
function sendemail(xName,xEmail,xRegType,language){
	$.ajax({
		url : base_url+'ajax.php',
		type : 'POST',
		data : 'request=ajaxsendemailprocess&var_name='+xName+'&var_email='+xEmail+'&var_register='+xRegType+'&var_base_url='+base_url+'&lang_param='+language,
		dataType : 'json',		
		success : function(data) {
			$("#name").val("");
			$("#email").val("");
			showdialog(data.msg);
		}
	});
}
// Show Dialog
function showdialog($msg) {
	$("#enlist-message .message-text").fadeIn().text($msg);
	$(".enlist-message").click();
}
// Check valid email address
function checkEmail(email)
{
	var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (!filter.test(email))
	{
		return false;
	}
	return true;
}
// Check if empty Field
function empty (mixed_var)
{
    var key;

    if (mixed_var === "" || mixed_var === 0 || mixed_var === "0" || mixed_var === null || mixed_var === false || typeof mixed_var === 'undefined') {
        return true;
    }

    if (typeof mixed_var == 'object') {
        for (key in mixed_var) {
            return false;
        }
        return true;
    }

    return false;
}