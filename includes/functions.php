<?php

function saveToDatabase($site_id,$sText){
	$sSql = "INSERT INTO `minisite_contest` (`mc_user`, `mc_minisite`, `mc_value`) VALUES ('1', '".$site_id."', '".$sText."')";
    $GLOBALS["db"]->query($sSql);
	
	$decode_inserted_data = json_decode($sText,true);
	
	return getLastEntry($site_id,$decode_inserted_data['email']);
	
}

function getAllEntries($sid){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$sid.' AND `mc_value` LIKE \'%"fname"%\' ORDER BY RAND() ';
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$decode_votes = json_decode($row['mc_value'],true);
		$arr_entry_list[] = array('id'=>$row['mc_id'],'data'=>$row['mc_value']);	
	}
	
	return $arr_entry_list;
	
}

function searchRecordsToDBUnicode($sid,$s_sval){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT * FROM `minisite_contest` 
				WHERE mc_minisite = '.$sid.' 
				AND `mc_value` LIKE \'%"fname"%\' AND `mc_value` LIKE \'%"'.str_replace('%', '\\\\\\\\', $s_sval).'%\' 
				ORDER BY `mc_timestamp` DESC
			';
	
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$decode_recs = json_decode($row['mc_value'],true);
		$arr_entry_list[] = array('id'=>$row['mc_id'],'data'=>$row['mc_value']);	
	}
	
	return $arr_entry_list;
	
}

function searchRecordsToDBRecCountsUnicode($sid,$s_sval){
	
	$arr_entry_list_count = 0;
	
	$sSql = 'SELECT count(*) as search_rec_count FROM `minisite_contest` 
				WHERE mc_minisite = '.$sid.' 
				AND `mc_value` LIKE \'%"fname"%\' AND `mc_value` LIKE \'%"'.str_replace('%', '\\\\\\\\', $s_sval).'%\' 
				ORDER BY `mc_timestamp` DESC
			';
			
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$arr_entry_list_count = $row['search_rec_count'];	
	}
	
	return $arr_entry_list_count;
	
}

function searchRecordsToDB($sid,$s_sval){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT * FROM `minisite_contest` 
				WHERE mc_minisite = '.$sid.' 
				AND `mc_value` LIKE \'%"fname"%\' AND lower(`mc_value`) LIKE \'%"'.strtolower($s_sval).'%\' 
				ORDER BY `mc_timestamp` DESC
			';
			
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$decode_recs = json_decode($row['mc_value'],true);
		$arr_entry_list[] = array('id'=>$row['mc_id'],'data'=>$row['mc_value']);	
	}
	
	return $arr_entry_list;
	
}

function searchEmailToUnsubscribe($sid,$s_sval){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT * FROM `minisite_contest` 
				WHERE mc_minisite = '.$sid.' 
				AND `mc_value` LIKE \'%"email"%\' AND `mc_value` LIKE \'%"'.$s_sval.'"%\' 
			';
			
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$decode_recs = json_decode($row['mc_value'],true);
		$arr_entry_list[] = array('id'=>$row['mc_id'],'data'=>$row['mc_value']);	
	}
	
	return $arr_entry_list;
	
}

function searchRecordsToDBRecCounts($sid,$s_sval){
	
	$arr_entry_list_count = 0;
	
	$sSql = 'SELECT count(*) as search_rec_count FROM `minisite_contest` 
				WHERE mc_minisite = '.$sid.' 
				AND `mc_value` LIKE \'%"fname"%\' AND lower(`mc_value`) LIKE \'%"'.strtolower($s_sval).'%\' 
				ORDER BY `mc_timestamp` DESC
			';
			
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$arr_entry_list_count = $row['search_rec_count'];	
	}
	
	return $arr_entry_list_count;
	
}

/* My Codes */

function sendemailprocess($var_email,$var_regtype,$var_url,$lang_param){
	include('texts/'.$lang_param.'.php');
	$gameloftLink = array(
		'en' => 'http://www.gameloft.com/',
		'ko' => 'http://www.gameloft.com/', //expired domain
		'jp' => 'http://www.gameloft.co.jp/',
	);	
	
	$localeVar = array(
		'en' => array(
					'gl_link' => 'http://www.gameloft.com/',
					'fb_locale' => 'en_US',
					'tw_locale' => 'en',
				),
		'ko' => array(
					'gl_link' => 'http://www.gameloft.com/',
					'fb_locale' => 'ko_KR',
					'tw_locale' => 'ko',
				),
		'jp' => array(
					'gl_link' => 'http://www.gameloft.co.jp/',
					'fb_locale' => 'ja_JP',
					'tw_locale' => 'ja',
				),
		'fr' => array(
					'gl_link' => 'http://www.gameloft.fr/',
					'fb_locale' => 'fr_FR',
					'tw_locale' => 'fr',
				),
		'sp' => array(
					'gl_link' => 'http://www.gameloft.es/',
					'fb_locale' => 'es_ES',
					'tw_locale' => 'es',
				),
		'sp-latam' => array(
					'gl_link' => 'http://www.gameloft.es/',
					'fb_locale' => 'es_LA',
					'tw_locale' => 'es',
				),
		'ar' => array(
					'gl_link' => 'http://www.gameloft.ae/',
					'fb_locale' => 'ar_AR',
					'tw_locale' => 'ar',
				),
		'de' => array(
					'gl_link' => 'http://www.gameloft.de/',
					'fb_locale' => 'de_DE',
					'tw_locale' => 'nl',
				),
		'it' => array(
					'gl_link' => 'http://www.gameloft.it/',
					'fb_locale' => 'it_IT',
					'tw_locale' => 'it',
				),
		'ru' => array(
					'gl_link' => 'http://www.gameloft.com/',
					'fb_locale' => 'ru_RU',
					'tw_locale' => 'ru',
				),
		'br' => array(
					'gl_link' => 'http://br.gameloft.com/',
					'fb_locale' => 'pt_BR',
					'tw_locale' => 'pt',
				),
	);	
	$ar_indicator = '';
	if($lang_param == 'ar')
		$ar_indicator = 'direction:rtl;';
		
	$like_image = $lang_param;

	if($lang_param == 'sp-latam')
		$like_image = 'sp';		
		
	mb_internal_encoding('UTF-8');
	$to = trim($var_email);
	$subject = mb_encode_mimeheader($aTexts['newsletter']['subject'], 'UTF-8', 'Q');
	
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
	$headers .= 'From: '.mb_encode_mimeheader($aTexts['share']['facebook_header'], 'UTF-8', 'Q').' <registration@dungeonhunter5.com>' . "\r\n";
	
	// $getHTMLContent = file_get_contents( 'html/autoreply.html' );
	
	$message = '
<!DOCTYPE html>
<head>
<title>DH5 Newsletter</title>
<style type="text/css">
	* { margin:0; padding:0; font-family: "Arial", "Helvetica"; }
	#emailblast { width:500px; max-width:500px; margin: 0 auto; }
	.e-b-footer { font-size:12px; }
	a { border:none; outline:none; }
	.gl-logo, .gl-logo img { display:block; outline:none; border:none; }
	@media only screen and (max-width: 500px) {
		#emailblast { width:100% !important; }
		#emailblast table { width:100% !important; }
	}
	@media only screen and (max-width: 480px) {
		#emailblast { width:100%; }
		#emailblast table { width:100% !important; }
	}
</style>
</head>
<body>
	<div id="emailblast">
		<table bgcolor="#000000" width="480" align="center" cellspacing="0" cellpadding="0" border="0" style="width:480px">
			<tr>
				<td class="e-b-viewing" bgcolor="#161514" align="center" cellspacing="0" cellpadding="0" border="0" style="padding:15px;color:#999999;font-family:\'Arial\',\'Helvetica\';font-size:15px;">'.$aTexts['newsletter']['trouble_view'].' <strong style="text-decoration:underline;'.$ar_indicator.'"><a href="'.URL_DINAMIC_CONTENT.'html/preview.php?email='.$to.'&l='.base64_encode(base64_encode($lang_param)).'" style="color:#999999;">'.$aTexts['newsletter']['web_version'].'</a></strong>'.$aTexts['newsletter']['trouble_view_after'].'</td>
			</tr>
			<tr>
				<td class="e-b-banner"><img src="https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/banner_'.$lang_param.'.jpg" width="500" height="311" alt="" title="" /></td>
			</tr>
			<tr>
				<td style="padding:0px 10px 15px 10px;">
					<h1 class="e-b-congrats" style="color:#f4f1c1;font-size:17px;font-weight:normal;line-height:1.0;text-align:center;font-family:\'Arial\',\'Helvetica\';'.$ar_indicator.'">'.$aTexts['newsletter']['congrats'].'</h1>
				</td>
			</tr>
			<tr>
				<td style="padding:0px 37px;"><div style="border-bottom:1px solid #7a7b7b;"></div></td>
			</tr>
			<tr>
				<td class="e-b-powers"><img src="https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/powerups.jpg" width="500" height="348" alt="" title="" /></td>
			</tr>
			<tr>
				<td style="padding:0px 37px;"><div style="border-bottom:1px solid #7a7b7b;"></div></td>
			</tr>
			<tr>
				<td style="padding:20px 30px 10px 30px;background-color:#000;background-image:url(\'https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/bg_text.jpg\');background-position:center;">
					<h1 class="e-b-congrats" style="color:#f4f1c1;font-size:17px;font-weight:normal;line-height:1.0;text-align:center;font-family:\'Arial\',\'Helvetica\';'.$ar_indicator.'">'.$aTexts['newsletter']['spread'].'</h1>
				</td>
			</tr>
			<tr>
				<td style=\'padding:22px 0px; text-align:center;\'>
					<span style=\'display:inline-block;padding:5px 2px;\'><a href=\'https://www.facebook.com/DungeonHunter\' target=\'_blank\' style="outline:none;border:0;" border="0"><img style="outline:none;border:0;" src="https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/like_fb_'.$like_image.'.png" width="116" height="64" alt="" title="" /></a></span>
				</td>
			</tr>
			<tr>
				<td class="e-b-footer" style="min-height:100px;padding:20px 10px 5px 10px; background-color:#000;background-image:url("https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/bg_footer.jpg");background-repeat:no-repeat;'.$ar_indicator.'">
					<table width="480" align="center" cellspacing="0" cellpadding="0" border="0" >
						<tr>
							<td style="padding:0px 10px;"><a class="gl-logo" style="display:block;outline:none;border:none;" href="'.$localeVar[$lang_param]['gl_link'].'"><img style="display:block;outline:none;border:none;" src="https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/images/autoreply/gameloft.png" width="76" height="53" alt="" title="" /></a></td>
							<td>
								<table style="color:#999999;padding:10px;border-left:1px solid #7b7b7b;font-size:11px;font-family:\'Arial\',\'Helvetica\';">
									<tr>
										<td style="'.$ar_indicator.'">'.$aTexts['newsletter']['footer'].'</td>
									</tr>
									<tr>
										<td style="padding-top:12px;">
											<p style="text-align:right;"><a href="'.URL_DINAMIC_CONTENT.'?unsub='.$to.'&l='.base64_encode(base64_encode($lang_param)).'" style="text-decoration:underline;color:#999999;">'.$aTexts['newsletter']['unsubscribe'].'</a></p>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</body>
</html>	
	';
	
	@mail ($to, $subject, $message, $headers);	
}

function generate_validation_code($ms_id){
	
	$var_temp_code = '';
	$var_str = '';
	
	for($i=0;$i < VALIDATION_LENGTH;$i++){
		$var_str = RANDOM_ALPHANUM;
		$var_temp_code .= $var_str[mt_rand(1,strlen(RANDOM_ALPHANUM))];
	}
	
	if(validate_generated_code($ms_id,$var_temp_code)){
		return $var_temp_code;
	}
	
}

function validate_generated_code($ms_id,$var_temp_code){
	
	$sSql = 'SELECT count(*) as code_count FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"'.trim($var_temp_code).'"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$arr_code_list_count = $row['code_count'];	
	}
	
	if($arr_code_list_count > 0){
		generate_validation_code($ms_id);	
	} else {
		return true;
	}
	
}

function validate_email_address($ms_id,$var_email,$var_regtype){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT count(*) as email_count FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"'.trim($var_email).'"%\' AND `mc_value` LIKE \'%"'.trim($var_regtype).'"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$arr_email_list_count = $row['email_count'];
	}
	
	if($arr_email_list_count > 0){
		return true;	
	} else {
		return false;
	}
	
}

function check_unsubscribed_user($ms_id,$var_email){
	
	$arr_entry_list = array();
	
	$sSql = 'SELECT count(*) as email_count FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"'.trim($var_email).'"%\' AND `mc_value` LIKE \'%"deleted":1%\' LIMIT 1 ';
	$GLOBALS["db"]->query($sSql);
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		$arr_email_list_count = $row['email_count'];
	}
	
	if($arr_email_list_count > 0){
		return true;	
	} else {
		return false;
	}
	
}

function validate_confimation_code($ms_id,$var_code){
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"'.trim($var_code).'"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	return $GLOBALS['db']->fetchAssoc();
	
}

function update_confirm_registration($var_data,$var_rec_id){
		
	$sSql = "UPDATE `minisite_contest` SET `mc_value`= '".$var_data."' WHERE `mc_id`=".$var_rec_id." ";
	$GLOBALS["db"]->query($sSql);
	
	return true;
}

function update_unsubscribe_email($var_data,$var_rec_id){
		
	$sSql = "UPDATE `minisite_contest` SET `mc_value`= '".$var_data."' WHERE `mc_id`=".$var_rec_id." ";
	$GLOBALS["db"]->query($sSql);
	
	return true;
}

function insert_email_record($ms_id,$var_details){
		
	$sSql = "INSERT INTO `minisite_contest` (`mc_user`, `mc_minisite`, `mc_value`) VALUES ('0', '".$ms_id."', '".$var_details."')";
    $GLOBALS["db"]->query($sSql);
	
    return true;
}

function get_total_points($ms_id){
	
	$sPoints = 0;
	$sDecText = '';
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"reg_confirmed":1%\' ';
	$GLOBALS["db"]->query($sSql);
	
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		extract($row);
		$sDecText = json_decode($mc_value,true);
		$sPoints += floatval($sDecText['points']);
	}
	
	return $sPoints;
}

function get_display_login($var_msg=''){
	
	if(empty($var_msg)){
		$str_dislay_error_messages = '';
	} else {
		$str_dislay_error_messages = '
		<p>
			<span>Invalid credentails!</span><BR/>
			<span>Please provide a valid credentials.</span>
		</p>
		';
	}
	
	$str_display_login = '';
	$str_display_login .= '
		<div class="points-cont dh-ab dh-center">
			<div class="po-c-center dh-set dh-middle">
				<div class="login-form par2">
					<ul>
						<li><input type="text" name="txt_uname" id="txt_uname" placeholder="Username"></li>
						<li><input type="password" name="txt_upass" id="txt_upass" placeholder="Password"></li>
						<li>
							<div class="dh-set"><button id="btn_login" name="btn_login">Login</button></div>
							'.$str_dislay_error_messages.'
						</li>
					</ul>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	';
	// <div class="dh-set"><button id="btn_cancel" name="btn_cancel">Cancel</button></div>
	return $str_display_login;
}

function get_display_points($ms_id){
	$str_display_points = '';
	$str_points_display .= '
	<div class="po-c-ctrl dh-ab">
		<p>Welcome Admin, <a href="#" id="btn_logout">Logout</a></p>
	</div>
	<div class="points-cont dh-ab dh-center">
		<div class="po-c-center dh-set">
			<div class="po-c-counter">
				<div class="po-c-point-set dh-set">
					<div class="dh-set"><label id="crp_points">'.get_total_points_exceptions($ms_id).'</label></div>
					<p><strong>Current Registration Points</strong></p>
				</div>
				<div class="po-c-point-set dh-set">
					<span class="dh-set"><label id="map_points">'.get_admin_points($ms_id).'</label></span>
					<p><strong>Manually Added Points</strong></p>
				</div>
				<div class="clr"></div>
			</div>
			<div class="po-c-field dh-center">
				<div class="dh-set">
					<div class="fl"><input type="text" name="txt_points" id="txt_points" onkeypress="return isNumberKey(event)" value="" placeholder="Points to be added"></div>
					<div class="fl"><input type="submit" name="btn_points" id="btn_points" value="Save Points"></div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
	';
	
	return $str_points_display;
}

function get_admin_points($ms_id){
		
	$str_return_val = 0;
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"reg_type":"admin"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	$row = $GLOBALS['db']->fetchAssoc();
	
	if(count($row) > 0){
		$arr_decode = json_decode($row['mc_value'],true);
		$str_return_val = $arr_decode['points'];
	} else {
		$str_return_val = 0;
	} 
	
	return $str_return_val;
	
}

function validate_admin_points($ms_id){
		
	$str_return_val = 0;
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"reg_type":"admin"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	$row = $GLOBALS['db']->fetchAssoc();
	
	return count($row);
	
}

function get_total_points_exceptions($ms_id){
	
	$sPoints = 0;
	$sDecText = '';
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND (`mc_value` LIKE \'%"reg_type":"twitter"%\' OR `mc_value` LIKE \'%"reg_type":"facebook"%\' OR `mc_value` LIKE \'%"reg_type":"website"%\' OR `mc_value` LIKE \'%"reg_type":"mobile"%\') AND `mc_value` LIKE \'%"reg_confirmed":1%\' ';
	$GLOBALS["db"]->query($sSql);
	
	
	while($row = $GLOBALS['db']->fetchAssoc()){
		extract($row);
		$sDecText = json_decode($mc_value,true);
		$sPoints += floatval($sDecText['points']);
	}
	
	return $sPoints;
}

function get_admin_points_arr($ms_id){
	
	$sSql = 'SELECT * FROM `minisite_contest` WHERE mc_minisite = '.$ms_id.' AND `mc_value` LIKE \'%"reg_type":"admin"%\' ';
	$GLOBALS["db"]->query($sSql);
	
	$row = $GLOBALS['db']->fetchAssoc();
	
	$arr_decode = json_decode($row['mc_value'],true);
	$str_return_val = $arr_decode['points'];
	$str_return_id = $row['mc_id'];

	return array('mid'=>$str_return_id,'mpoints'=>$str_return_val);

}

function update_manual_points($var_data,$var_rec_id){
		
	$sSql = "UPDATE `minisite_contest` SET `mc_value`= '".$var_data."' WHERE `mc_id`=".$var_rec_id." ";
	$GLOBALS["db"]->query($sSql);
	
	return true;
} 

 
?>