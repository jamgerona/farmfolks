<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = '¡Gracias por tu preinscripción! ¡Te informaremos en cuanto Dungeon Hunter 5 y tus recompensas estén disponibles!';
$aTexts['err']['email_fail'] = 'Esta dirección de correo electrónico ya está registrada.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Registro completado.';
$aTexts['err']['email_conf_2'] = 'Tu código de confirmación ya se ha confirmado.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'La unión hace la fuerza. Únete a mí y ayúdame a desbloquear recompensas exclusivas en Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = '¡Gracias por compartir! Por favor, comparte de nuevo para conseguir más puntos.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'Ya has compartido esto.';
$aTexts['err']['valid_email_empty'] = 'Dirección de correo electrónico necesaria.';
$aTexts['err']['valid_email'] = 'Has introducido una dirección de correo electrónico que no es válida.';
$aTexts['err']['privacy_policy'] = 'Por favor, acepta la política de privacidad y los términos y condiciones.';
$aTexts['err']['email_unsubscribe'] = "Ya hemos dado de baja la suscripción asociada a tu correo electrónico.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Inicio';

$aTexts['home']['prove_your_worth'] = 'DEMUESTRA TU VALOR';

$aTexts['home']['prove_your_worth_p_1'] = '<p>Detener la invasión demoniaca fue tan solo el principio del fin. 
Fue más de lo que pudimos soportar... el reino se hizo añicos, los vientos dispersaron a los ciudadanos... De la gloria de antaño de Valenthia solo queda una sombra fantasmal.</p>';

$aTexts['home']['band_together'] = '<strong>Uníos</strong> como una fuerza de cazarrecompensas. <strong>Alistaos</strong>, <strong>y haced</strong> correr la voz, <strong>reclutad</strong> a otros guerreros, y <strong>reunid recompensas exclusivas</strong> para estar equipados cuando llegue el momento de enfrentaros al mal.';

$aTexts['home']['email'] = 'Correo electrónico'; //placeholder
$aTexts['home']['share'] = 'Compartir:';
$aTexts['home']['tweet'] = 'Compartir:';
$aTexts['home']['enlist'] = 'Alistar:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'Tengo 13 años o más. Acepto los <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=es" target="_blank">términos de uso</a></span> y he leído <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=es" target="_blank">la política de privacidad</a></span>';

$aTexts['home']['watch_first'] = '¡Observa el regreso del legendario Dungeon Hunter!';
$aTexts['home']['watch_now'] = 'images/home/watch_now_sp.png';

$aTexts['home']['concept_art_t'] = 'Arte conceptual';
$aTexts['home']['concept_art_d'] = '¡Descubre y disfruta de un montón de ilustraciones exclusivas, diseñadas por los artistas del juego durante las primeras fases de su desarrollo!';
$aTexts['home']['fusion_booster_t'] = 'Potenciadores de fusión:';
$aTexts['home']['fusion_booster_d'] = "El éter natural de la tierra ha sido extraído mediante métodos prohibidos para crear poderosos objetos. ¡Úsalos para impregnar tus armas de poderes devastadores!";
$aTexts['home']['gold_t'] = 'Oro';
$aTexts['home']['gold_d'] = "Hasta en los tiempos más oscuros de Valenthia, el oro se mantiene como el idioma universal del comercio y la política. Cuando las palabras son mudas, el oro se hace oír.";
$aTexts['home']['gems_t'] = 'Gemas';
$aTexts['home']['gems_d'] = 'No hay nada más valioso para los mercaderes de Valenthia que el brillo de las gemas. Solo con ellas podrás alcanzar el botín más raro.';
$aTexts['home']['minion_t'] = 'Soldados';
$aTexts['home']['minion_d'] = "Parte del éxito de los clanes de cazarrecompensas radica en su capacidad de domesticar monstruos y sobornar a los enemigos y a guardias de las fortalezas secretas. Consigue a los tuyos y asegúrate de mantener tu botín a salvo de los ataques de los asaltantes codiciosos.";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. Todos los derechos reservados. Gameloft y el logotipo de Gameloft son marcas registradas de Gameloft en EE. UU. y/u otros países.<br/>Las demás marcas registradas son propiedad de sus respectivos dueños.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Inscripción - Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = '¿Tienes problemas para ver este correo electrónico? Puedes ver la ';
$aTexts['newsletter']['web_version'] = 'versión web';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = '<strong>Enhorabuena, acabas de unirte a los clanes de los cazarrecompensas</strong>, ofreciendo una oportunidad a todos los guerreros para estar mejor equipados cuando llegue el momento de la batalla.';
$aTexts['newsletter']['spread'] = '<strong>¡Haz correr la voz</strong>, recluta a tus amigos <strong>y reúne un ejército de cazarrecompensas</strong> lo suficientemente fuerte como para recibir como recompensa el soldado más terrorífico!';
$aTexts['newsletter']['unsubscribe'] = 'Cancelar suscripción';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. Todos los derechos reservados. Gameloft y el logotipo de Gameloft son marcas registradas de Gameloft en los EE. UU. y/u otros países.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>