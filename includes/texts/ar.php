<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'شكرًا على تسجيلك المسبق! سنقوم بتنبيهك عندما تتوفر لعبة Dungeon Hunter 5 والجوائز الخاصة بك.';
$aTexts['err']['email_fail'] = 'بريدك الإلكتروني مسجل بالفعل.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'لقد أنهيت عملية التسجيل.';
$aTexts['err']['email_conf_2'] = 'تم تأكيد رمز التأكيد الخاص بك.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'الكثرة تغلب الشحاعة. انضم إليّ وساعدني على فتح قفل الجوائز الحصرية في Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'شكرًا على المشاركة! استمر بالمشاركة لتحرز مزيدًا من النقاط.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'لقد شاركت هذا المحتوى من قبل.';
$aTexts['err']['valid_email_empty'] = 'بريدك الإلكتروني مطلوب.';
$aTexts['err']['valid_email'] = 'لقد أدخلت بريدًا إلكترونيًا غير صحيح.';
$aTexts['err']['privacy_policy'] = 'يرجى الموافقة على سياسة الخصوصية والشروط والأحكام.';
$aTexts['err']['email_unsubscribe'] = "تم غلغاء اشتراك بريدك الإلكتروني بالفعل.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | الرئيسية';

$aTexts['home']['prove_your_worth'] = 'اثبت قوّتك';

$aTexts['home']['prove_your_worth_p_1'] = '<p>لم يكن إيقاف غزو العفاريت إلا بداية للمخاطر الكبيرة التي ستأتي بعده...فقد انهارت المملكة وتشتت سكانها وذهبت أمجاد فالينثيا مع الريح...</p>';

$aTexts['home']['band_together'] = '<strong>تعاون</strong> مع صائدي الجوائز <strong>وانشر الخبر</strong> وقم بتعيين المحاربين <strong>واجمع</strong> الجوائز الحصرية لتواجه الشر بكل قوة.';

$aTexts['home']['email'] = 'البريد الإلكتروني'; //placeholder
$aTexts['home']['share'] = 'مشاركة:';
$aTexts['home']['tweet'] = 'مشاركة:';
$aTexts['home']['enlist'] = 'انضمام:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'عمري يتجاوز 13 عامًا وأوافق على <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=ar" target="_blank">الشروط والأحكام</a></span> وقد قرأت <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=ar" target="_blank">سياسة الخصوصية</a></span>';

$aTexts['home']['watch_first'] = 'شاهد عودة Dunegon Hunter بعد طول انتظار!';
$aTexts['home']['watch_now'] = 'images/home/watch_now_ar.png';

$aTexts['home']['concept_art_t'] = 'فنون اللعبة:';
$aTexts['home']['concept_art_d'] = 'اكتشف واستمتع بحزمة مليئة بالرسوم الحصرية والتي صممها فنانو اللعبة أثناء المراحل الأولى من تطويرها!';
$aTexts['home']['fusion_booster_t'] = 'تعزيزات الدمج:';
$aTexts['home']['fusion_booster_d'] = "تم استخراج أثير الأرض الطبيعي عبر وسائل ممنوعة بهدف صناعة هذه الأغراض القوية. استخدمها لتعزز قوة أسلحتك!";
$aTexts['home']['gold_t'] = 'الذهب';
$aTexts['home']['gold_d'] = "حتى في أصعب ظروف فالينثيا، يظل الذهب هو اللغبة الرسمية المستخدمة في التجارة والسياسة. لا صوت أعلى من صوت الذهب!";
$aTexts['home']['gems_t'] = 'الأحجار الكريمة';
$aTexts['home']['gems_d'] = 'عشق الأحجار الكريمة يسيطر بالفعل على جميع تجار فالينثيا! باستخدامها، ستحصل على أروع وأندر الغنائم!';
$aTexts['home']['minion_t'] = 'الأتباع';
$aTexts['home']['minion_d'] = "من أهم أسباب نجاح نقابات صيادي الجوئز هو قدرتهم على ترويض الوحوش وتقديم الرشاوى لأعدائهم لكي يحرسوا حصونهم السرية. احصل على حصنك واحمي غنائمك من هجمات الغزاة الطماعين!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. جميع الحقوق محفوظة. Gameloft وشعار Gameloft علامات تجارية مملوكة لشركة Gameloft ومسجلة في الولايات المتحدة و\أو خارجها. <br/>جميع العلامات التجارية الأخرى مملوكة لمالكي حقوقها.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'التسجيل في Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = 'هل تواجه مشكلة في قراءة هذه الرسالة؟ اطلع على ';
$aTexts['newsletter']['web_version'] = ' نسخة الويب.';
$aTexts['newsletter']['trouble_view_after'] = '';

$aTexts['newsletter']['congrats'] = '<strong>تهانينا! لقد انضممت لنقابات صائدي الجوائزs</strong> وستساعد جميع المقاتلين على الاستعداد جيدًا للمعركة القادمة!';
$aTexts['newsletter']['spread'] = 'انشر الخبر</strong> وقم بتعيين أصدقائك <strong>واحشد جيشًا قويًا من صيادي الجوائز</strong> لكي تفوز بأقوى الأتباع وأكثرهم رعبًا!';
$aTexts['newsletter']['unsubscribe'] = 'إلغاء الاشتراك';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. جميع الحقوق محفوظة. Gameloft وشعار Gameloft علامات تجارية مملوكة لشركة Gameloft في الولايات المتحدة و\أو خارجها.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>