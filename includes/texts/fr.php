<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'Merci pour votre préinscription ! Lorsque Dungeon Hunter 5 et vos récompenses seront disponibles, nous vous préviendrons !';
$aTexts['err']['email_fail'] = "L'adresse e-mail est déjà enregistrée.";//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Votre inscription est terminée.';
$aTexts['err']['email_conf_2'] = 'Votre code de confirmation a déjà été confirmé.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = "L'union fait la force ! Rejoignez-moi et débloquons des récompenses exclusives dans Dungeon Hunter 5 !";
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = "Merci d'avoir partagé ! Partagez à nouveau pour gagner plus de points.";
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'Vous avez déjà partagé ceci.';
$aTexts['err']['valid_email_empty'] = "L'adresse e-mail est requise.";
$aTexts['err']['valid_email'] = 'Vous avez entré une adresse e-mail invalide.';
$aTexts['err']['privacy_policy'] = 'Veuillez accepter la politique de confidentialité et les conditions générales.';
$aTexts['err']['email_unsubscribe'] = "Votre adresse e-mail est déjà désinscrite.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Accueil';

$aTexts['home']['prove_your_worth'] = 'Prouvez votre valeur';

$aTexts['home']['prove_your_worth_p_1'] = "<p>Arrêter l'invasion des démons n'était que le début de la fin. C'en était trop. Le royaume s'effondra, laissant des habitants livrés à eux-mêmes. La gloire de Valenthia n'est plus qu'un lointain souvenir.
</p>";

$aTexts['home']['band_together'] = "<strong>Alliez-vous</strong> à d'autres chasseurs de primes. <strong>Engagez-vous</strong>, <strong>faites-vous connaître</strong>, <strong>recrutez d'autres</strong> guerriers et <strong>obtenez des récompenses exclusives</strong> qui vous permettront de faire face aux forces maléfiques.";

$aTexts['home']['email'] = 'E-mail'; //placeholder
$aTexts['home']['share'] = 'Partager :';
$aTexts['home']['tweet'] = 'Partager :';
$aTexts['home']['enlist'] = "S'engager :";

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = "J'ai plus de 13 ans. J'accepte les <span class='dh-set'><a href='http://www.gameloft.com/conditions/?lang=fr' target='_blank'>conditions générales</a></span> et j'ai lu la <span class='dh-set'><a href='http://www.gameloft.com/privacy-notice/?lang=fr' target='_blank'>politique de confidentialité</a></span>. ";

$aTexts['home']['watch_first'] = 'Regardez le retour du légendaire Dungeon Hunter ! ';
$aTexts['home']['watch_now'] = 'images/home/watch_now_fr.png';

$aTexts['home']['concept_art_t'] = 'Ébauches :';
$aTexts['home']['concept_art_d'] = "Découvrez un pack d'illustrations exclusives des graphistes du jeu, datant des premières phases de développement !";
$aTexts['home']['fusion_booster_t'] = 'Boosts de fusion :';
$aTexts['home']['fusion_booster_d'] = "L'éther naturel du territoire a été extrait avec des moyens interdits pour créer ces puissants items. Utilisez-les pour imprégner vos armes d'une puissance dévastatrice !";
$aTexts['home']['gold_t'] = 'Or';
$aTexts['home']['gold_d'] = "Alors que Valenthia est en crise, l'or reste le langage universel du commerce et de la politique. Quand la parole ne suffit plus, l'or a le dernier mot !";
$aTexts['home']['gems_t'] = 'Gemmes';
$aTexts['home']['gems_d'] = "Il n'y a rien de plus précieux pour les marchands de Valenthia que de brillantes gemmes polies. Et elles seules vous permettront de mettre la main sur les butins les plus rares !";
$aTexts['home']['minion_t'] = 'Sbires';
$aTexts['home']['minion_d'] = "Les guildes de chasseurs de primes doivent en partie leur succès à leur capacité à dompter les monstres et à soudoyer leurs ennemis pour en faire leurs propres soldats, gardiens de leurs forteresses secrètes. Recrutez-en pour protéger votre butin des autres chasseurs de primes avides de trésors !";

$aTexts['footer']['copyright'] = "&copy;2015 Gameloft. Tous droits réservés. Gameloft et le logo Gameloft sont des marques déposées de Gameloft aux États-Unis et/ou dans d'autres pays. <br/>Toutes les autres marques déposées sont la propriété de leurs détenteurs respectifs.";

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Inscription Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = "Problème d'affichage ? Accédez à la ";
$aTexts['newsletter']['web_version'] = 'version web';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = "<strong>Félicitations, vous venez de rejoindre les rangs des guildes de chasseurs de primes</strong>, et vous offrez une chance à tous vos semblables d'être mieux équipés lorsque la bataille fera rage !";
$aTexts['newsletter']['spread'] = '<strong>Faites-vous connaître</strong>, recrutez vos amis, et <strong>bâtissez une armée de chasseurs de primes</strong> suffisamment puissante pour être récompensée avec le plus terrifiant des sbires !';
$aTexts['newsletter']['unsubscribe'] = 'Se désabonner';
$aTexts['newsletter']['footer'] = "&copy; 2015 Gameloft. Tous droits réservés. Gameloft et le logo Gameloft sont des marques déposées de Gameloft aux États-Unis et/ou dans d'autres pays.";

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>