<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = '¡Gracias por tu preinscripción! ¡Te informaremos cuando Dungeon Hunter 5 y tus recompensas estén disponibles!';
$aTexts['err']['email_fail'] = 'Esta dirección de correo electrónico ya está registrada.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Tu registro está completo.';
$aTexts['err']['email_conf_2'] = 'Tu código de confirmación ya se confirmó.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'La unión hace la fuerza. Únete a mí y ayúdame a desbloquear recompensas exclusivas en Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = '¡Gracias por compartir! Por favor comparte de nuevo para conseguir más puntos.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'Ya compartiste esto.';
$aTexts['err']['valid_email_empty'] = 'Se requiere dirección de correo electrónico.';
$aTexts['err']['valid_email'] = 'Introdujiste una dirección de correo electrónico no válida.';
$aTexts['err']['privacy_policy'] = 'Por favor acepta la política de privacidad y los términos y condiciones.';
$aTexts['err']['email_unsubscribe'] = "Ya cancelamos la suscripción de tu correo electrónico.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Inicio';

$aTexts['home']['prove_your_worth'] = 'DEMUESTRA CUÁNTO VALES';

$aTexts['home']['prove_your_worth_p_1'] = '<p>Detener la invasión demoniaca fue tan solo el principio del fin. 
Fue más de lo que pudimos soportar... el reino se hizo añicos, la gente se dispersó... De la gloria de antaño de Valenthia solo queda una sombra fantasmal.</p>';

$aTexts['home']['band_together'] = '<strong>Únete</strong> como una fuerza de cazarrecompensas. <strong>Enrólate</strong>, y <strong>riega</strong> la voz, <strong>recluta</strong> otros guerreros, y <strong>recoge recompensas exclusivas</strong> para estar equipados cuando llegue el momento de enfrentarte al mal.';

$aTexts['home']['email'] = 'Correo electrónico'; //placeholder
$aTexts['home']['share'] = 'Compartir:';
$aTexts['home']['tweet'] = 'Compartir:';
$aTexts['home']['enlist'] = 'Enrolar:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'Tengo más de 13 años. Acepto los  <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=es" target="_blank">términos y condiciones</a></span> y he leído la <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=es" target="_blank">política de privacidad</a></span>';

$aTexts['home']['watch_first'] = '¡Observa el regreso del legendario Dungeon Hunter!';
$aTexts['home']['watch_now'] = 'images/home/watch_now_sp-latam.png';

$aTexts['home']['concept_art_t'] = 'Arte conceptual:';
$aTexts['home']['concept_art_d'] = '¡Descubre y disfruta de un montón de ilustraciones exclusivas, diseñadas por los artistas del juego durante las primeras fases del proceso de desarrollo!';
$aTexts['home']['fusion_booster_t'] = 'Potenciadores de fusión:';
$aTexts['home']['fusion_booster_d'] = "El éter natural de la tierra ha sido extraído mediante métodos prohibidos para crear estos poderosos objetos. ¡Úsalos para impregnar tus armas con poderes devastadores!";
$aTexts['home']['gold_t'] = 'Oro';
$aTexts['home']['gold_d'] = "Aún en los tiempos más oscuros de Valenthia, el oro sigue siendo el idioma universal del comercio y la política. Cuando las palabras fracasan, ¡el oro prevalece!";
$aTexts['home']['gems_t'] = 'Gemas';
$aTexts['home']['gems_d'] = 'No hay nada más valioso para los mercaderes de Valenthia que el brillo de las gemas pulidas. Solo con ellas podrás obtener el botín más raro.';
$aTexts['home']['minion_t'] = 'Compinches';
$aTexts['home']['minion_d'] = "Parte del éxito de los clanes de cazarrecompensas radica en su capacidad para domesticar monstruos y sobornar a los enemigos para que vigilen sus fortalezas secretas. ¡Consigue los tuyos y asegúrate de mantener tu botín a salvo de los ataques de saqueadores codiciosos!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. Todos los derechos reservados. Gameloft y el logotipo de Gameloft son marcas registradas de Gameloft en EE.UU. y/u otros países.<br/>Las demás marcas registradas son propiedad de sus respectivos dueños.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Inscripción - Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = '¿Tienes problemas para ver este correo electrónico? Puedes ver la ';
$aTexts['newsletter']['web_version'] = 'versión web';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = '<strong>Felicitaciones acabas de unirte a las filas de los clanes de cazarrecompensas</strong>, lo que les ofrece una oportunidad a todos los guerreros para estar mejor equipados cuando llegue el momento de la batalla.';
$aTexts['newsletter']['spread'] = '<strong>¡Riega la voz</strong>, recluta a tus amigos <strong>y reúne un ejército de cazarrecompensas</strong> lo suficientemente fuerte como para recibir como recompensa el compinche más terrorífico!';
$aTexts['newsletter']['unsubscribe'] = 'Cancelar suscripción';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. Todos los derechos reservados. Gameloft y el logotipo de Gameloft son marcas registradas de Gameloft en los EE. UU. y/u otros países.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>