<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'Thank You for your pre-registration! When Farm Folks and your rewards are available we will let you know!';
$aTexts['err']['email_fail'] = 'Email address is already registered.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Your registration is complete.';
$aTexts['err']['email_conf_2'] = 'Your confirmation code is already confirmed.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'Strength comes in numbers, join me and help unlock exclusive rewards in Dungeon Hunter 5';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'Thanks for sharing! Please share again to earn more points.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'You already shared this.';
$aTexts['err']['valid_email_empty'] = 'Email address is required.';
$aTexts['err']['valid_name_empty'] = 'Name Field is required.';
$aTexts['err']['valid_email'] = 'You have entered an invalid email address.';
$aTexts['err']['privacy_policy'] = 'Please accept Privacy Policy and Terms and Conditions.';
$aTexts['err']['email_unsubscribe'] = "You're email is already unsubscribed.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Home';

$aTexts['home']['prove_your_worth'] = 'PROVE YOUR WORTH';

$aTexts['home']['prove_your_worth_p_1'] = '<p>Stopping the demon invasion was only the beginning of the end. It was more than we could take -- the kingdom shattered, scattering its people to the winds, the former glory of Valenthia now just a fading ghost.</p><p>We try to rebuild, but our roads are taken by raiders, our swamps overrun with demons, our forests filled with ravenous beasts, and even as we speak the fallen soldiers of the war rise up as undead to join ranks against the living. It is a dark time, and the future is bleak. Only the bounty hunter guilds thrive, the remnants of a broken military, providing the only measure of safety in the land, and only for those that can afford it. I wonder... will you be among those devoured by the chaos that eats this land, bounty hunter? Or will you rise above the rest and be the one that evil comes to fear?</p>';

$aTexts['home']['band_together'] = '<strong>Band together</strong> as a force of Bounty Hunters. <strong>Enlist</strong>, <strong>share</strong> the word, <strong>recruit</strong> fellow warriors, and <strong>collect exclusive rewards</strong> to come equipped when time comes to face evil.';

$aTexts['home']['email'] = 'E-mail'; //placeholder
$aTexts['home']['share'] = 'Share:';
$aTexts['home']['tweet'] = 'Share:';
$aTexts['home']['enlist'] = 'Enlist:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'I am over 13. I agree to the <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=en" target="_blank">Terms and Conditions</a></span> and I have read the <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=en" target="_blank">Privacy Policy</a></span>';

$aTexts['home']['watch_first'] = 'Watch the Return of the legendary Dungeon Hunter! ';
$aTexts['home']['watch_now'] = 'images/home/watch_now.png';

$aTexts['home']['concept_art_t'] = 'Concept Art:';
$aTexts['home']['concept_art_d'] = 'Discover and enjoy a pack of exclusive illustrations , designed by the game’s artists during the early stages of the development process!';
$aTexts['home']['fusion_booster_t'] = 'Fusion Boosters:';
$aTexts['home']['fusion_booster_d'] = "The land's natural aether has been extracted through forbidden means to create these powerful items. Use them to imbue your weapons with devastating powers!";
$aTexts['home']['gold_t'] = 'Gold';
$aTexts['home']['gold_d'] = "Even in Valenthia's darkest hour, gold remains the universal language of commerce and politics. When words fail, gold prevails!";
$aTexts['home']['gems_t'] = 'Gems';
$aTexts['home']['gems_d'] = 'There is nothing more precious to the merchants of Valenthia than the glow of polished gems. Only with these can you attain the rarest loot!';
$aTexts['home']['minion_t'] = 'Minions';
$aTexts['home']['minion_d'] = "Part of the Bounty Hunter Guilds' success is due to their ability to tame monsters and bribe enemies into standing guard in their secret strongholds. Get yours and make sure to keep your loot safe from the attacks of greedy raiders!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. All rights reserved. Gameloft and the Gameloft logo are trademarks of Gameloft in the U.S. and/or other countries. <br/>All other trademarks are the property of their respective owners.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Dungeon Hunter 5 Registration';
$aTexts['newsletter']['trouble_view'] = 'Trouble viewing this email? View the ';
$aTexts['newsletter']['web_version'] = 'web version.';
$aTexts['newsletter']['trouble_view_after'] = '';

$aTexts['newsletter']['congrats'] = '<strong>Congratulations, you just joined the ranks of the Bounty Hunter guilds</strong>, offering a chance to all fellow warriors to come better equipped when the time for battle comes!';
$aTexts['newsletter']['spread'] = '<strong>Spread the word</strong>, hire your friends, <strong>and gather an army of Bounty Hunters</strong> strong enough to get rewarded with the most terrifying minion!';
$aTexts['newsletter']['unsubscribe'] = 'Unsubscribe';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. All Rights Reserved. Gameloft and the logo Gameloft are trademarks of Gameloft in the US and/or other countries.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>