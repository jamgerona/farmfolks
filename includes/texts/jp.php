<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = '事前登録が完了しました。ゲームの配信後、登録メールアドレスに案内をお送りいたします。';
$aTexts['err']['email_fail'] = '既にお客さまのEメールアドレスは登録されています。';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = '登録が完了しました';
$aTexts['err']['email_conf_2'] = '確認コードは既に確認済みです';

$aTexts['share']['facebook_header'] = 'Dark Quest 5（ダーククエスト5）';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = '力を合わせて戦おう！『Dark Quest 5（ダーククエスト5）』に参加してスペシャル報酬をアンロックしよう！';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'シェアしました！またシェアしてポイントを稼ぎましょう！';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = '既にシェア済みです';
$aTexts['err']['valid_email_empty'] = 'Eメールアドレスが入力されていません。';
$aTexts['err']['valid_email'] = '不正なEメールアドレスです。';
$aTexts['err']['privacy_policy'] = '個人情報（Eメールアドレス）収集に同意していません。もう一度ご確認ください。';
$aTexts['err']['email_unsubscribe'] = "お客様のメールアドレスは既に登録解除されています";

$aTexts['home']['title'] = 'Dark Quest 5（ダーククエスト5）| Home';

$aTexts['home']['prove_your_worth'] = '<img src="images/jp/dh_quote.png" width="625" height="203" alt="" title="" />';

$aTexts['home']['prove_your_worth_p_1'] = '<p>デーモンたちとの戦いに傷ついたバレンシア王国。荒廃した領土は内乱の危機に瀕し、かつてのバレンシアの栄光は息絶えようとしていた…。
</p>';

$aTexts['home']['band_together'] = '<span class="big">勇気ある者たちよ、結集せよ！</span><br/><br/>事前登録して情報を広め、仲間を集めよ！<br/>事前登録者数が増えれば増えるほど豪華報酬を受け取ることができるだろう!<br/>豪華報酬をゲットして戦いに備えよ!';

$aTexts['home']['email'] = 'PC用メールアドレス入力'; //placeholder
$aTexts['home']['share'] = 'シェア';
$aTexts['home']['tweet'] = 'いいね';
$aTexts['home']['enlist'] = '事前登録する';

// For JP only
$aTexts['home']['popuplink'] = '個人情報（Eメールアドレス）収集に同意 <span style="color:#FFF">（必須）</span>';

$aTexts['home']['over_13'] = '<a id="details" class="dh-cur">詳細</a> <a href="http://www.gameloft.com/privacy-notice/?from=IGA&igagame=D4HM&ctg=PRIVACY&ver=1.6.0&lang=JP&hitId=9254208690 " target="_blank">プライバシーポリシー</a>';

$aTexts['home']['watch_first'] = '<img src="images/jp/wacth_quote.png" width="345" height="87" alt="" title="" />';
$aTexts['home']['watch_now'] = 'images/jp/watch_now.png';

$aTexts['home']['concept_art_t'] = 'コンセプトアート：';
$aTexts['home']['concept_art_d'] = 'ゲームの開発初期に制作された未公開<br/>コンセプトアート集。<br/><span style="color:#FFF">※ご登録いただいたメール宛にお送りいたします。</span>';
$aTexts['home']['fusion_booster_t'] = '合成ブースター：';
$aTexts['home']['fusion_booster_d'] = "禁じられた方法で抽出された大地の<br/>エーテルによって作られた強力なアイテム。<br/>武器と合成して強力な力を吹き込もう。<br/><span style='color:#FFF'>※ゲーム開始時に通常時の２倍の量を配布いたします。</span>";
$aTexts['home']['gold_t'] = 'ゴールド：';
$aTexts['home']['gold_d'] = "暗黒時代のバレンシアでもゴールドは<br/>政治と取引使われる共通の言語である。<br/>ゴールドは言葉に勝る!<br/><span style='color:#FFF'>※ゲーム開始時に通常時の２倍の量を配布いたします。</span>";
$aTexts['home']['gems_t'] = 'ジェム：';
$aTexts['home']['gems_d'] = 'バレンシアの商人にとって光り輝くジェムほど<br/>魅力を放つものはない。<br/>希少な戦利品はジェムによってのみ入手できる!<br/><span style="color:#FFF">※ゲーム開始時に通常時の２倍の量を配布いたします。</span>';
$aTexts['home']['minion_t'] = 'ミニオン：';
$aTexts['home']['minion_d'] = "賞金稼ぎたちのギルドはモンスターを飼いならす<br/>能力を持ち、敵に賄賂を渡して自分たちの要塞を<br/>守ることでその隆盛を保っている。<br/>ミニオンを使って欲深い侵入者たちから戦利品を<br/>守ろう。<br/><span style='color:#FFF'>※ゲーム開始時に配布するチケットを使用して入手することができます。</span>";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. All rights reserved. Gameloft and the Gameloft logo are trademarks of Gameloft in the U.S. and/or other countries. <br/>All other trademarks are the property of their respective owners.';

/* Newsletter */
$aTexts['newsletter']['subject'] = '「Dark Quest 5」登録';
$aTexts['newsletter']['trouble_view'] = 'メールが読めない場合は';
$aTexts['newsletter']['web_version'] = 'ウェブ版';
$aTexts['newsletter']['trouble_view_after'] = 'をご覧ください。';

$aTexts['newsletter']['congrats'] = '<strong>おめでとう！賞金稼ぎのギルドのメンバーになりました！</strong>戦いの時が来るまでに仲間と共に戦闘準備を整えましょう！';
$aTexts['newsletter']['spread'] = '<strong>みんなに声をかけて</strong>フレンドを雇ったり、<strong>賞金稼ぎの仲間を集めたり</strong>して恐ろしい敵との戦いに備えよう！';
$aTexts['newsletter']['unsubscribe'] = '登録解除';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. All Rights Reserved. Gameloft and the logo Gameloft are trademarks of Gameloft in the US and/or other countries.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>