<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'Grazie di esserti registrato! Ti faremo sapere quando Dungeon Hunter 5 e i tuoi premi saranno disponibili!';
$aTexts['err']['email_fail'] = 'Indirizzo e-mail già registrato.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'La registrazione è stata completata.';
$aTexts['err']['email_conf_2'] = 'Il codice di conferma è già confermato.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'Insieme siamo più forti! Unisciti a me e sblocca premi esclusivi di Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'Grazie per la condivisione! Condividi ancora per guadagnare altri punti.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = "L'hai già condiviso.";
$aTexts['err']['valid_email_empty'] = 'È necessario inserire un indirizzo e-mail.';
$aTexts['err']['valid_email'] = 'Hai inserito un indirizzo e-mail non valido.';
$aTexts['err']['privacy_policy'] = 'Accetta la politica sulla privacy e i termini e le condizioni.';
$aTexts['err']['email_unsubscribe'] = "Il tuo indirizzo e-mail è già stato rimosso.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Pagina iniziale';

$aTexts['home']['prove_your_worth'] = 'DIMOSTRA QUANTO VALI';

$aTexts['home']['prove_your_worth_p_1'] = "<p>Fermare l'invasione dei demoni non è stato altro che l'inizio della fine. Eravamo andati troppo oltre... Il regno era in frantumi, il popolo in ginocchio. Valenthia era ormai il fantasma della sua antica gloria.</p>";

$aTexts['home']['band_together'] = '<strong>Metti insieme</strong> una squadra di cacciatori di taglie. <strong>Partecipa</strong>, <strong>condividi</strong>, <strong>recluta</strong> guerrieri poderosi e <strong>raccogli premi esclusivi</strong> da usare quando sarà il momento di affrontare il male.';

$aTexts['home']['email'] = 'E-mail'; //placeholder
$aTexts['home']['share'] = 'Condividi:';
$aTexts['home']['tweet'] = 'Condividi:';
$aTexts['home']['enlist'] = 'Partecipa:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'Ho più di 13 anni. Accetto i <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=it" target="_blank">Termini di utilizzo</a></span> e dichiaro di aver letto le <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=it" target="_blank">Politiche sulla privacy</a></span>';

$aTexts['home']['watch_first'] = "Guarda il video sul ritorno del leggendario Dungeon Hunter!";
$aTexts['home']['watch_now'] = 'images/home/watch_now_it.png';

$aTexts['home']['concept_art_t'] = 'Concept art:';
$aTexts['home']['concept_art_d'] = 'Scopri un pacchetto di illustrazioni esclusive, ideate dagli artisti del gioco durante le prime fasi di sviluppo!';
$aTexts['home']['fusion_booster_t'] = 'Bonus unione:';
$aTexts['home']['fusion_booster_d'] = "L'etere naturale della terra è stato estratto con rituali proibiti per creare questi potenti oggetti. Usali per donare alle tue armi poteri devastanti!";
$aTexts['home']['gold_t'] = 'Monete';
$aTexts['home']['gold_d'] = "Nell'ora più scura di Valenthia, l'oro rimane il linguaggio universale del commercio e della politica. Anche quando la diplomazia fallisce, l'oro trionfa sempre!";
$aTexts['home']['gems_t'] = 'Gemme';
$aTexts['home']['gems_d'] = "Per i mercanti di Valenthia, non c'è niente di più prezioso del luccichio delle gemme. Le gemme sono l'unica cosa che ti permetterà di ottenere il bottino più raro!";
$aTexts['home']['minion_t'] = 'Servitori';
$aTexts['home']['minion_d'] = "Il successo delle gilde di cacciatori di taglie è dovuto anche alla loro abilità di domare e assoldare i mostri per difendere le loro fortezze segrete. Raduna i tuoi servitori e proteggi il tuo bottino dagli attacchi di nemici avidi di ricchezze!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. Tutti i diritti riservati. Gameloft e il logo Gameloft sono marchi registrati di Gameloft negli Stati Uniti e/o in altri paesi.<br />Tutti i marchi registrati appartengono ai rispettivi proprietari.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Registrazione a Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = 'Problemi nella visualizzazione di questa e-mail? Vedi la ';
$aTexts['newsletter']['web_version'] = 'versione web';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = "<strong>Congratulazioni, sei entrato a far parte della gilda dei cacciatori di taglie</strong>, dando così l'occasione a un tuo compagno di ottenere equipaggiamento migliore per le battaglie future!";
$aTexts['newsletter']['spread'] = '<strong>Spargi la voce</strong>, recluta i tuoi amici, <strong>e raduna un esercito di cacciatori di taglie</strong> in grado di raccogliere i servitori più terrificanti!';
$aTexts['newsletter']['unsubscribe'] = 'Annulla iscrizione';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. Tutti i diritti riservati. Gameloft e il logo Gameloft sono marchi registrati di Gameloft negli Stati Uniti e/o in altri paesi.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>