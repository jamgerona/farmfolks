<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'Спасибо за предварительную регистрацию! Когда выйдет Dungeon Hunter 5 и откроются награды, мы вам сообщим!';
$aTexts['err']['email_fail'] = 'Такой адрес электронной почты уже зарегистрирован.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Регистрация завершена.';
$aTexts['err']['email_conf_2'] = 'Код подтверждения уже введен.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'Сила в количестве. Присоединяйтесь и помогите открыть эксклюзивные награды в Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'Спасибо, что рассказали! Расскажите снова и заработайте больше очков.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'Вы уже рассказали об этом.';
$aTexts['err']['valid_email_empty'] = 'Введите адрес электронной почты.';
$aTexts['err']['valid_email'] = 'Вы ввели адрес недействующей электронной почты.';
$aTexts['err']['privacy_policy'] = 'Примите Политику конфиденциальности и Условия использования';
$aTexts['err']['email_unsubscribe'] = "Вы отказались от подписки на данный адрес электронной почты.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Главная';

$aTexts['home']['prove_your_worth'] = 'Докажите, что вы достойны';

$aTexts['home']['prove_your_worth_p_1'] = '<p>Вторжение демонов было остановлено, но это лишь начало конца. Королевство разрушено. Былая слава Валентии исчезает, словно призрак.</p><p>Мы пытаемся восстановить наши города, но дороги захвачены разбойниками, болота кишат демонами, а леса переполнены кровожадными тварями. И прямо сейчас павшие в битве воины снова встают, но уже как нежить, и идут против живых. Тёмное время. Будущее неясно. Лишь только гильдия охотников за головами способствует тому, чтобы хоть какой-то порядок оставался на землях Валентии. Будешь ли ты в рядах тех, кто уничтожает свой мир, будучи одурманенным влиянием сил хаоса? Или ты станешь тем, от чьего взгляда самые тёмные души побегут прочь?</p>
';

$aTexts['home']['band_together'] = 'Охотники за головами, <strong>собирайте силы</strong>. <strong>Подписывайтесь,</strong> <strong>делитесь</strong> новостями, <strong>находите</strong> союзников и <strong>собирайте эксклюзивные трофеи,</strong> чтобы встретиться со злом во всеоружии.';

$aTexts['home']['email'] = 'Email'; //placeholder
$aTexts['home']['share'] = 'Поделиться:';
$aTexts['home']['tweet'] = 'Поделиться:';
$aTexts['home']['enlist'] = 'Подписаться:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'Мне уже есть 13. Я принимаю условия <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=ru" target="_blank">Пользовательского соглашения</a></span> и подтверждаю, что ознакомился с <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=ru" target="_blank">Политики конфиденциальности.</a></span>';

$aTexts['home']['watch_first'] = 'Не пропустите возвращение легендарной игры Dungeon Hunter! ';
$aTexts['home']['watch_now'] = 'images/home/watch_now_ru.png';

$aTexts['home']['concept_art_t'] = 'Иллюстрации:';
$aTexts['home']['concept_art_d'] = 'Насладитесь эксклюзивными иллюстрациями, созданными художниками игры на ранних стадиях процесса ее разработки!';
$aTexts['home']['fusion_booster_t'] = 'Усиления синтеза:';
$aTexts['home']['fusion_booster_d'] = "Это мощное оружие было создано из природных сил самой земли с помощью запретного искусства. Оно наделит вас невероятной силой!";
$aTexts['home']['gold_t'] = 'Золото';
$aTexts['home']['gold_d'] = "Даже в самые тяжелые времена Валентии золото остается универсальным языком торговли и политики. Золото справится там, где слова оказались бессильны!";
$aTexts['home']['gems_t'] = 'Алмазы';
$aTexts['home']['gems_d'] = 'Для валентийских торговцев нет ничего прекраснее сияния отполированных алмазов. Только их вы сможете обменять на небывалые редкости!';
$aTexts['home']['minion_t'] = 'Клевреты';
$aTexts['home']['minion_d'] = "Секрет успеха Гильдии охотников за головами в их умении порабощать монстров и подкупать врагов, превращая их в стражей своих тайных убежищ. Используйте их, чтобы ваша добыча не попала в загребущие бандитские лапы!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. Все права защищены.
Gameloft и логотип Gameloft являются товарными знаками Gameloft в США и/или других странах.<br/>Все другие торговые марки являются собственностью их владельцев.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Регистрация в Dungeon Hunter  5.';
$aTexts['newsletter']['trouble_view'] = 'Не можете прочитать письмо? Попробуйте ';
$aTexts['newsletter']['web_version'] = 'web-версию';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = '<strong>Поздравляем, вы вступили в ряды Гильдии охотников за головами,</strong>, и теперь ваши соратники вступят в бой более подготовленными!';
$aTexts['newsletter']['spread'] = '<strong>Делитесь новостями,</strong> вербуйте друзей <strong>и собирайте армию охотников за головами </strong> настолько сильную, чтобы получить в награду самого жуткого клеврета!';
$aTexts['newsletter']['unsubscribe'] = 'Отписаться';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. Все права защищены. Gameloft и логотип Gameloft являются товарными знаками Gameloft в США и/или других странах.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>