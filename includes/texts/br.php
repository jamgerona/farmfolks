<?php
/* Home */
$aTexts['url']['short_link'] = 'http://gmlft.co/EfPTo';
$aTexts['url']['devdiary_link'] = 'https://www.youtube.com/watch?v=yJCsAxoe4uo';

$aTexts['err']['email_send'] = 'Obrigado pelo seu pré-cadastro! Quando Dungeon Hunter 5 e suas recompensas estiverem disponíveis, nós o avisaremos!';
$aTexts['err']['email_fail'] = 'Este endereço de e-mail já foi registrado.';//'Registration failed, your email is either already registered or not valid.';
$aTexts['err']['email_conf'] = 'Seu cadastro está completo.';
$aTexts['err']['email_conf_2'] = 'Seu código de confirmação já foi confirmado.';

$aTexts['share']['facebook_header'] = 'Dungeon Hunter 5';
$aTexts['share']['facebook_title'] = $aTexts['share']['facebook_header'].'.';
$aTexts['share']['facebook_desc'] = 'A união faz a força. Junte-se a mim para desbloquear recompensas exclusivas em Dungeon Hunter 5.';
$aTexts['share']['twitter_message'] = urlencode($aTexts['share']['facebook_desc'].' '.$aTexts['url']['short_link'].' ');

$aTexts['err']['twitter_share'] = 'Obrigado por compartilhar! Compartilhe novamente para ganhar mais pontos.';
$aTexts['err']['facebook_share'] = $aTexts['err']['twitter_share'];
$aTexts['err']['facebook_fail'] = 'Você já compartilhou isso.';
$aTexts['err']['valid_email_empty'] = 'É necessário um endereço de e-mail.';
$aTexts['err']['valid_email'] = 'Você digitou um endereço de e-mail inválido.';
$aTexts['err']['privacy_policy'] = 'Por favor, aceite a política de privacidade e os termos e condições de uso';
$aTexts['err']['email_unsubscribe'] = "Seu e-mails já foi removido.";

$aTexts['home']['title'] = 'Dungeon Hunter 5 | Home';

$aTexts['home']['prove_your_worth'] = 'Prove seu valor';

$aTexts['home']['prove_your_worth_p_1'] = '<p>Acabar com a invasão de demônios foi apenas o começo do fim. Foi mais do que podíamos aguentar - o reino destruído, as pessoas espalhadas aos ventos, a antiga glória de Valenthia reduzida a um pálido fantasma.</p>';

$aTexts['home']['band_together'] = '<strong>Una-se a outros</strong>, formando uma força de caçadores de recompensas. <strong>Aliste</strong>, <strong>divulgue</strong>, <strong>recrute</strong> guerreiros e <strong>recolha recompensas exclusivas</strong> para estar bem preparado quando chegar a hora de enfrentar o mal.';

$aTexts['home']['email'] = 'E-mail'; //placeholder
$aTexts['home']['share'] = 'Compartilhe:';
$aTexts['home']['tweet'] = 'Compartilhe:';
$aTexts['home']['enlist'] = 'Aliste:';

// For JP only
$aTexts['home']['popuplink'] = 'with popup link';

$aTexts['home']['over_13'] = 'Tenho mais de 13 anos de idade. Eu concordo com os <span class="dh-set"><a href="http://www.gameloft.com/conditions/?lang=br" target="_blank">Termos e Condições</a></span> e li a <span class="dh-set"><a href="http://www.gameloft.com/privacy-notice/?lang=br" target="_blank">Política de Privacidade</a></span>.';

$aTexts['home']['watch_first'] = 'Veja o retorno do lendário Dungeon Hunter';
$aTexts['home']['watch_now'] = 'images/home/watch_now_br.png';

$aTexts['home']['concept_art_t'] = 'Arte conceitual:';
$aTexts['home']['concept_art_d'] = 'Descubra e aprecie um conjunto de ilustrações exclusivas feitas pelos artistas do jogo durante as primeiras fases do processo de desenvolvimento!';
$aTexts['home']['fusion_booster_t'] = 'Acessórios de fusão:';
$aTexts['home']['fusion_booster_d'] = "O éter natural do território foi extraído por meio de processos proibidos para criar esses itens poderosos. Use-os para conceder poderes devastadores às suas armas!";
$aTexts['home']['gold_t'] = 'Ouro';
$aTexts['home']['gold_d'] = "Mesmo nos momentos mais obscuros de Valenthia, o ouro continua sendo o idioma universal do comércio e da política. Quando as palavras falham, o ouro prevalece!";
$aTexts['home']['gems_t'] = 'Joias';
$aTexts['home']['gems_d'] = 'Não há nada mais precioso para os mercadores de Valenthia do que o brilho de joias polidas. Somente com elas você consegue os prêmios mais raros!';
$aTexts['home']['minion_t'] = 'Criaturas';
$aTexts['home']['minion_d'] = "Parte do sucesso das guildas de caçadores de recompensas se deve à sua habilidade de domar monstros e subornar inimigos para ficarem de vigia em suas fortalezas secretas. Tenha os seus e assegure-se de manter seu tesouro a salvo de ataques de assaltantes gananciosos!";

$aTexts['footer']['copyright'] = '&copy;2015 Gameloft. Todos os direitos reservados. Gameloft e o logo da Gameloft são marcas registradas da Gameloft nos E.U.A. e/ou em outros países. <br/>Todas as outras marcas registradas são propriedade de seus respectivos donos.';

/* Newsletter */
$aTexts['newsletter']['subject'] = 'Registro do Dungeon Hunter 5';
$aTexts['newsletter']['trouble_view'] = 'Problemas ao exibir este e-mail? Acesse a ';
$aTexts['newsletter']['web_version'] = 'versão web';
$aTexts['newsletter']['trouble_view_after'] = '.';

$aTexts['newsletter']['congrats'] = '<strong>Parabéns, você acaba de se juntar às fileiras das guildas de caçadores de recompensas</strong>, dando uma chance a todos os companheiros guerreiros de se equiparem melhor para a futura batalha!';
$aTexts['newsletter']['spread'] = '<strong>Compartilhe</strong>, contrate seus amigos <strong>e forme um exército de caçadores de recompensas</strong> forte o suficiente para ser recompensado com a criatura mais aterradora! ';
$aTexts['newsletter']['unsubscribe'] = 'Cancelar assinatura';
$aTexts['newsletter']['footer'] = '&copy; 2015 Gameloft. Todos os direitos reservados. Gameloft e a marca Gameloft são marcas registradas da Gameloft nos E.U A. e /ou em outros países.';

/* Korea */
$aTexts['error']['enternumber'] = 'Please enter your phone number.';
$aTexts['error']['numberonly'] = 'Only numbers can be entered.';
$aTexts['error']['selectplatform'] = 'Please select platform';

$aTexts['home']['ios'] = 'iOS';
$aTexts['home']['android'] = 'Android';
?>