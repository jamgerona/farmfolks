<?php

// Avoid hardcoding the absolute path to this file
$sPath =  dirname (__FILE__).'/';

// load configuration for this minisite
require ($sPath.'local.config.php');
require ($sPath.'functions.php');


// check if the config data exists
if ( ! isset ($aConfig)  || ! isset ($aConfig['paths']['fs_common_files']) )
{
    throw new Exception ('The common files path is not defined!');
}

// load the Cminisite class
require_once ($aConfig['paths']['fs_common_files'].'/Extended_CMinisite.php');


$oModel = new Extended_CMinisite ($aConfig);

// Localize the minisite ( set country id and language id)
$oModel->localizeMinisite ();

//$sSection = $oModel->getSection ();
$sLocalization = $oModel->getLocalization ();

$sPlatforms = $oModel->getPlatforms ();

if(isset($aConfig['platforms']['splash']) && $aConfig['platforms']['default'] == $oModel->getPlatforms ())
{
    $sPlatforms = $aConfig['platforms']['splash'];
}
else
{
    $sPlatforms = $oModel->getPlatforms ();
}

$country = $oModel->getCountryId();

$sAdid = $oModel->getCurrentAdid ();

$buyIphone = "";

$GLOBALS['db'] = new Database(false);

if ( DEBUG == true)
			$GLOBALS['db']->connect( DB_SHOWCASE_TEST_SERVER, DB_SHOWCASE_TEST_SERVER_USERNAME, DB_SHOWCASE_TEST_SERVER_PASSWORD, DB_SHOWCASE_TEST_DATABASE, false, true );
		else
			$GLOBALS['db']->connect( DB_SHOWCASE_SERVER, DB_SHOWCASE_SERVER_USERNAME, DB_SHOWCASE_SERVER_PASSWORD, DB_SHOWCASE_DATABASE, false, true );
?>