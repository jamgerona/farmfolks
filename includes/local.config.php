<?php
if ( ! isset ($sPath) )
{
    $sPath = dirname (__FILE__).'/';
}

define ('DEBUG', true);

define('SECRET_WORD','farm-folks');
define('FB_LINK','https://www.facebook.com/FarmFolksGame');

if (DEBUG){
// GAMELOFTSHOP NIGHTY (TEST)
    define('MY_DB_SHOWCASE_TEST_SERVER', 'db-web'); // h-shopdb
    define('MY_DB_SHOWCASE_TEST_SERVER_USERNAME','shopnewuser');
    define('MY_DB_SHOWCASE_TEST_SERVER_PASSWORD','DD-41_o0-3');
    define('MY_DB_SHOWCASE_TEST_DATABASE','gameloftshop');
}else{
// GAMELOFTSHOP PRODUCTION

    define('MY_DB_SHOWCASE_SERVER', 'db-web'); // h-shopdb
    define('MY_DB_SHOWCASE_SERVER_USERNAME','shopnewuser');
    define('MY_DB_SHOWCASE_SERVER_PASSWORD','DD-41_o0-3');
    define('MY_DB_SHOWCASE_DATABASE','gameloftshop');

}

if ( DEBUG )
{
    //define ('URL_PANDORA', 'http://valpha.gameloft.com:20000'); // Alpha
    define ('URL_PANDORA', 'http://vgold.gameloft.com:20000'); // Production
    define ('URL_STATIC_CONTENT', 'http://media01-staging.mdc.gameloft.org/web_mkt/minisites/farm-folks-pre-reg/');
    define ('URL_DINAMIC_CONTENT', 'http://new-staging.gameloft.org/minisites/farm-folks-pre-reg/');
	define('FB_APP_ID',630271997075109);
}
else
{
    define ('URL_PANDORA', 'http://vgold.gameloft.com:20000'); // Production
    define ('URL_STATIC_CONTENT', 'https://media01.gameloft.com/web_mkt/minisites/dh5-prereg/');
    define ('URL_DINAMIC_CONTENT', 'http://www.dungeonhunter5.com/');
	define('FB_APP_ID',621825834612087);
}


$aConfig = array
(
    'paths' => array
    (
        'fs_common_files' => $sPath.'../../includes',
        'fs_base_website' => 'new.gameloft.com',
        'media_relative_path' => 'web_mkt/minisites/farm-folks-pre-reg/',
        'fs_texts_files' => $sPath.'texts/%%lang%%.php'
    ),
    'main' => array
    (
        'product_id' => 1062,
        'operation_id' => 666,
        'description_group' => array ( 50, 30 ),
        // default: USA = 223,
        'country_id' => 223,
        // default: American English = 6
        'language_id' => 6,
        'enable_redirection_by_user_agent' => false,
        'enable_debug' => true,
        'site_description' => 'farm-folks-pre-reg',
        'adid' => 144003,
        'style' => 'gl_overlay_black',
        'id' => 74
        //'platforms' => 'mobile'
    ),
    'mails' => array
    (
        // Comma separated email addresses
        'support_account' => ''
    ),
    'localizations' => array
    (
        'options' => array
        (
            'us' => array ( 'country_id' => 223, 'language_id' => 6, 'language_iso2' => 'us', 'country_name' => 'United States'),
        ),
        'variable' => 'lang',
        'default' => 'us'
    ),
    
    'user_agents' => array
    (
        'dont_redirect' => array ( 4402, 3126 )
    ),
    'gl_overlay_hide' => array
    (
        'facebook' => true,
        'twitter' => true,
        'youtube' => true,
        'news' => true,
        'glive' => true,
        'relatedGames' => true
    )
);
define('IS_MOBILE',false);
if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strpos($_SERVER['HTTP_USER_AGENT'], 'iPod') || strpos($_SERVER['HTTP_USER_AGENT'], 'Android') || strpos($_SERVER['HTTP_USER_AGENT'], 'iPad') || strpos(strtolower($_SERVER['HTTP_USER_AGENT']), 'windows phone'))) {
	define('IS_MOBILE',true);
}

/* ##### Added By Jerson 11182014 ##### */
define('VALIDATION_LENGTH',8);
define('RANDOM_ALPHANUM','abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWQYZ0123456789');

$aPrizePoints = array('website'=>10,'facebook'=>10,'twitter'=>5,'mobile'=>10,);

/* ##### End Here ##### */

/* Added by Sheryl */
define('FB_TITLE_NAME','Farm Folks');


/* loading Meter */
$def_loader_width = 36;
$max_loader_width = 640;
$max_points = 2000000;

/*
$max_loader_width = 675;
$max_points = 2040000;
*/
/* ##### Added By Jerson 11182014 ##### */
define('VALIDATION_LENGTH',8);
define('RANDOM_ALPHANUM','abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWQYZ0123456789');
define('MAINTENANCE_USERS',serialize(array('uname'=>'gladmin','upass'=>'gl@dm1n1!')));
/* End Added by Sheryl */
require_once('includes/texts/en.php');
$language_selected = 'en';
if(isset($_GET['l'])){
	if(file_exists('includes/texts/'.base64_decode(base64_decode($_GET['l'])).'.php')){
		require('includes/texts/'.base64_decode(base64_decode($_GET['l'])).'.php');
		$lang_param = base64_decode(base64_decode($_GET['l']));
		$_SESSION['l'] = base64_decode(base64_decode($_GET['l']));
		$language_selected = base64_decode(base64_decode($_GET['l']));
	}
	else
		require('includes/texts/en.php');
}

$localeVar = array(
	'en' => array(
				'gl_link' => 'http://www.gameloft.com/',
				'fb_locale' => 'en_US',
				'tw_locale' => 'en',
			),
	'ko' => array(
				'gl_link' => 'http://www.gameloft.com/',
				'fb_locale' => 'ko_KR',
				'tw_locale' => 'ko',
			),
	'jp' => array(
				'gl_link' => 'http://www.gameloft.co.jp/',
				'fb_locale' => 'ja_JP',
				'tw_locale' => 'ja',
			),
	'fr' => array(
				'gl_link' => 'http://www.gameloft.fr/',
				'fb_locale' => 'fr_FR',
				'tw_locale' => 'fr',
			),
	'sp' => array(
				'gl_link' => 'http://www.gameloft.es/',
				'fb_locale' => 'es_ES',
				'tw_locale' => 'es',
			),
	'sp-latam' => array(
				'gl_link' => 'http://www.gameloft.es/',
				'fb_locale' => 'es_LA',
				'tw_locale' => 'es',
			),
	'ar' => array(
				'gl_link' => 'http://www.gameloft.ae/',
				'fb_locale' => 'ar_AR',
				'tw_locale' => 'ar',
			),
	'de' => array(
				'gl_link' => 'http://www.gameloft.de/',
				'fb_locale' => 'de_DE',
				'tw_locale' => 'nl',
			),
	'it' => array(
				'gl_link' => 'http://www.gameloft.it/',
				'fb_locale' => 'it_IT',
				'tw_locale' => 'it',
			),
	'ru' => array(
				'gl_link' => 'http://www.gameloft.com/',
				'fb_locale' => 'ru_RU',
				'tw_locale' => 'ru',
			),
	'br' => array(
				'gl_link' => 'http://br.gameloft.com/',
				'fb_locale' => 'pt_BR',
				'tw_locale' => 'pt',
			),
);
?>